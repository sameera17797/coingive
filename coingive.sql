-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 22, 2018 at 08:53 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coingive`
--

-- --------------------------------------------------------

--
-- Table structure for table `CAMPAIGN`
--

CREATE TABLE `CAMPAIGN` (
  `CAM_ID` int(4) NOT NULL,
  `TARGET` int(5) NOT NULL,
  `C_ID` int(4) NOT NULL,
  `description` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;

--
-- Dumping data for table `CAMPAIGN`
--

INSERT INTO `CAMPAIGN` (`CAM_ID`, `TARGET`, `C_ID`, `description`) VALUES
(1, 1000, 2, ''),
(2, 1500, 1, ''),
(3, 8000, 7, ''),
(4, 4300, 11, ''),
(5, 3400, 10, ''),
(6, 4800, 12, ''),
(7, 5000, 18, ''),
(8, 4500, 11, ''),
(9, 5500, 19, ''),
(10, 5200, 14, ''),
(11, 6000, 17, ''),
(12, 5800, 16, ''),
(13, 5900, 15, ''),
(14, 6100, 11, ''),
(15, 5200, 12, ''),
(16, 5100, 13, ''),
(17, 5800, 14, ''),
(18, 5900, 9, ''),
(19, 6900, 8, ''),
(20, 6900, 5, ''),
(21, 100, 3, ''),
(22, 78, 23, ''),
(23, 0, 0, ''),
(24, 0, 0, ''),
(25, 0, 0, ''),
(26, 0, 0, ''),
(27, 100, 4, 'ftrt');

-- --------------------------------------------------------

--
-- Table structure for table `CHARITY`
--

CREATE TABLE `CHARITY` (
  `C_ID` int(4) NOT NULL,
  `NAME` varchar(20) NOT NULL,
  `EMAIL` varchar(20) NOT NULL,
  `PASSWORD` varchar(20) NOT NULL,
  `POSTCODE` int(4) NOT NULL,
  `C_WALLET` varchar(100) NOT NULL,
  `Description` longtext,
  `imagePath` varchar(200) NOT NULL DEFAULT 'sadaka.png'
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;

--
-- Dumping data for table `CHARITY`
--

INSERT INTO `CHARITY` (`C_ID`, `NAME`, `EMAIL`, `PASSWORD`, `POSTCODE`, `C_WALLET`, `Description`, `imagePath`) VALUES
(1, 'Salvos', 'salvos@gmail.com', 'salvos', 3177, '2MwxMqmpZ4F2Edvz5dmcDuLwL62oqTfFL17', 'Hello', '709spiderman-marvel-superhero-coloring-pages.jpg'),
(2, 'OP', 'op@gmail.com', 'op', 3127, 'bc1qq9lff86vn5ha295877hamurp6ftlr8mvta8g9t', 'The Brotherhood of St Laurence Store (op Shop) in the City is one of Melbourne’s best kept secrets, ', 'sadaka.png'),
(3, 'Same', 'sam@gmail.com', 'sameera', 3298, '1BeXBUnWYidytLpx58qQ9sdbvRSMA7V33i', 'Direct Relief is a humanitarian organization, active in all 50 states and more than 80 countries, wi', 'sadaka.png'),
(4, 'Kamal Foundations', 'kamalF@gmail.com', 'kamal', 3899, '3HJHZsGj9yLxNn5y6pp8uS4Tu9AcxqSho8', 'Matthew 25 Ministries (M25M) is an international humanitarian aid and disaster relief organization h', 'sadaka.png'),
(5, 'OP Darwin', 'op.Darwin@gmail.com', 'opdarwin', 2345, '3JDJeMUua24CxM26wMLSYYoNgUhGXkQCBF', 'Samaritan\'s Purse is a nondenominational evangelical Christian organization providing spiritual and ', 'sadaka.png'),
(7, 'OP Sunshine', 'opsunshine@op.com', 'sunshineop', 3189, '12EMnvsYBQZbhyByEwKjK3UmY8KzBdVS8g', 'The Rotary Foundation is a not-for-profit corporation funded solely by voluntary contributions from ', 'sadaka.png'),
(8, 'Salvos Sunshine', 'sunshinesal@salvos.c', 'sunshine', 3332, '33XS744Lw5m9wdWLCJzSXjzjrYwz2vpU8k', 'At The Conservation Fund, we believe that conservation should work for America. By creating solution', 'sadaka.png'),
(9, 'KKT HELP', 'dhana@kkthelp.com', 'dhanu', 6789, '31oLcVn3iFA1EfqS1qgJKqJRqWPNemsaMR', 'McGrath Breast Care Nurses help individuals (and their families) experiencing breast cancer by provi', 'sadaka.png'),
(10, 'Kct Help', 'aish@kctHelp.com', 'fiangi', 4455, '35qqSqZuvJqnmF8HFDx5g3hCw9UrVRo8SM', 'Berry Street is an independent Community Service Organisation and Australian charity. Established in', 'sadaka.png'),
(11, 'OP Derimmut', 'opderimut@op.com', 'sunshineop', 3180, '375iuUEm2An7FfDmRHwaK8X9QFafyQwgAs', NULL, 'sadaka.png'),
(12, 'Salvos Derimut', 'derimmut@salvos.com', 'derimut', 3334, '3MG5nSJBFvQJZLpv8ACr4QA7x2sLbUZzuM', NULL, 'sadaka.png'),
(13, 'Salvos Bundoora', 'bundoora@salvos.com', 'bundoora', 3304, '3Qe8P6TT5wj8nQvR42hrWZ7WiKRVx8XTCa', NULL, 'sadaka.png'),
(14, 'Church IIT', 'demon@churchIIT.com', 'Taylors', 3314, '3G8dBqxMGq7upqUHXTq1ShYJpaVjFmURc4', NULL, 'sadaka.png'),
(15, 'Doveton Savers', 'mainesht@dovetonsave', 'doveton', 3534, '1KhnL2F5osTgDz9h99bn1nJq4LKyfsQREK', 'Doveton Savers is the best place to buy stff as eavh goes to seperate chrity', 'sadaka.png'),
(16, 'Cranboune Help', 'suman@ch.com', 'prawn', 3181, '3NvFbQMAkkqEFj4i1sYPPLkSWzxTFUchjA', NULL, 'sadaka.png'),
(17, 'Pakenam Help', 'MAnie@ph.com', 'packy', 2334, '3JiDNSayg6KgSGyFNqBPzUZexQNCmRvZ8f', 'bbbbbbbbbbbb', 'sadaka.png'),
(18, 'Thomastown Charity', 'tom@th.com', 'thomas', 3404, '3CYQLVmym1KkHz7ouFydubo9EaBpScKbHK', NULL, 'sadaka.png'),
(19, 'St Kilda IIT', 'STK@churchIIT.com', 'Taylors44', 3340, '1P91pdDEVsqiMzehkYHPgLbCBckZNBvfeB', 'Yohu', 'sadaka.png'),
(20, 'Cranbourne Savers', 'cast@cranbournesaver', 'crane', 3594, '3LnyZk87EEP2UyuMMfaeb4GcwVjUW1HMec', 'Cranbourne savers are good', 'sadaka.png'),
(21, 'salvo', 's@gmail.com', 'sal', 2134, '1AJUD7qDrhrwnyBhVLWZGrZVKxDy89Db2c', NULL, 'sadaka.png'),
(22, 'Sam', 'sa13', 'ss', 22, '39N2DEohqyREWUHfaLSADKft8435r7zkQY', NULL, 'sadaka.png'),
(23, 'SAMEERA M GOUTHAM', 'sameera17797@gmail.c', 'jjjjj', 3177, '36VtyrdLJzXDLidH6CVGZnQFwKysELiLJS', 'Hello Sam', 'sadaka.png'),
(24, 'SAMEERA M GOUTHAM', 'sameera117797@gmail.', 'Sameera', 3177, '3QDeJZEeiRmSocc8crV9iXUWeRECuBUVUp', NULL, 'sadaka.png'),
(25, 'SAMEERA M GOUTHAM', 'sameera1217797@gmail', 'Sameerd', 3177, '16Uwg5Ptnj1U1uqoBdBgTfCou2SEHVmtW3', NULL, 'sadaka.png'),
(26, 'nameeeee', 'fsdf', '', 0, '155Z2kYbd6Pg8VmwTfbYhzjJkw5AdR94Kp', NULL, 'sadaka.png'),
(27, 'A Pakenaham', 'pakenham@gmail.com', 'pakehnam', 2345, '1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX', 'We are Pakehnam ', 'sadaka.png'),
(28, 'Saam', 'sasa', 'sss', 901, 'o000', NULL, 'sadaka.png'),
(29, 'SAmm', 'ss', 'ss', 2333, 'eee', NULL, 'sadaka.png'),
(30, 'Sameera', 'emal', 'ssss', 3333, 'dfaddggdgdg', NULL, 'sadaka.png'),
(31, 'Manasa', 'manasa@gmail.com', 'manasa', 3089, '83838ehhehehe8292202', 'Hey Myself Manasa Munni ', '4542Collage_Fotor.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `CONTACT`
--

CREATE TABLE `CONTACT` (
  `NAME` varchar(20) NOT NULL,
  `EMAIL` varchar(20) NOT NULL,
  `MESSEGE` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `CONTACT`
--

INSERT INTO `CONTACT` (`NAME`, `EMAIL`, `MESSEGE`) VALUES
('', '', ''),
('', '', ''),
('Sameera', 'sameera17797@gmail.c', 'Sameera'),
('Sameera', 'same@gmail.com', 'Hi I am working'),
('Kunda', 'kunda@gmail.com', 'kunda ia getting marreied'),
('', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `DONOR`
--

CREATE TABLE `DONOR` (
  `D_ID` int(4) NOT NULL,
  `NAME` varchar(80) CHARACTER SET ucs2 NOT NULL,
  `EMAIL` varchar(80) CHARACTER SET ucs2 NOT NULL,
  `PASSWORD` varchar(20) NOT NULL,
  `D_WALLET` varchar(20) CHARACTER SET ucs2 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `DONOR`
--

INSERT INTO `DONOR` (`D_ID`, `NAME`, `EMAIL`, `PASSWORD`, `D_WALLET`) VALUES
(1, 'Kamal', 'Kamal@gmail.com', 'kamal', 'daerds'),
(2, 'Hardik', 'hardik@gmail.com', 'hardik', 'fgrewss'),
(3, 'Kamal', 'kamal@gmail.co', 'kamal', '112234de'),
(4, 'Talha', 'talha@gmail.com', 'talha', 'gfhfhkk'),
(5, 'Sahere', 'sahere@gmail.com', 'sahare', 'gcgcnbb'),
(6, 'Ajeetha', 'ajjek@gmail.com', 'ajax', 'qqqqqqqq'),
(7, 'Karthik', 'Karthik@gmail.com', 'karthik', '1g1fhtfhkk'),
(8, 'Praveen', 'praveen@gmail.com', 'praveen', 'gcg2cnbdb234'),
(9, 'Ghat', 'Ghat@gmail.com', 'Ghat', 'gcgcn3u6bdb'),
(10, 'Manasa', 'manasa@gmail.com', 'manasa', 'g654cgcnbdb'),
(11, 'Vaidehi', 'vaidehi@gmail.com', 'vaidehi', 'gcgcnbdb4gt'),
(12, 'vishnu', 'vishnu@gmail.com', 'vishnu', 'huy9gcgcn6bdb'),
(13, 'vibhu', 'Vibhu@gmail.com', 'vibhu', 'gcgcn34b1db'),
(14, 'Amin', 'amin@gmail.com', 'amin', 'gc789gcnbd45b'),
(15, 'Halka', 'Halka@gmail.com', 'Halka', 'g5cgcnbd321b'),
(16, 'Bevarsi', 'Bevar223si@gmail.com', 'bevarsi', 'gcgce3nbdb23'),
(17, 'Nayi', 'Nayi@gmail.com', 'Nayi', 'gcgcnbdb345'),
(18, 'Munde', 'Munde@gmail.com', 'munde', 'gww3cgcnbdb'),
(19, 'Kachada', 'kachada@gmail.com', 'kachada', 'gcgcnbdeetryrb'),
(20, 'Kacha', 'Kacha@gmail.com', 'Kacha', 'gcgcnbww3db'),
(21, 'Kamal', 'kk', 'jjjjj', 'jsksksks'),
(22, 'Kaml', 'kjkkk', '0202j', 'dfdsv');

-- --------------------------------------------------------

--
-- Table structure for table `PAYMENT`
--

CREATE TABLE `PAYMENT` (
  `PAY_ID` int(4) NOT NULL,
  `PAY_TYPE` char(1) NOT NULL DEFAULT 'C',
  `D_ID` int(4) NOT NULL,
  `C_ID` int(4) NOT NULL,
  `C_WALLET` varchar(20) NOT NULL,
  `D_WALLET` varchar(20) NOT NULL,
  `TOTAL_COIN` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;

--
-- Dumping data for table `PAYMENT`
--

INSERT INTO `PAYMENT` (`PAY_ID`, `PAY_TYPE`, `D_ID`, `C_ID`, `C_WALLET`, `D_WALLET`, `TOTAL_COIN`) VALUES
(1, 'C', 1, 1, '12345', 'daerds', 6),
(2, 'C', 2, 2, '123456', 'fgrewss', 2),
(3, 'C', 2, 2, '123456', 'fgrewss', 3),
(4, 'C', 6, 14, '2Bkj1jfjfkfklf', 'qqqqqqqq', 67),
(5, 'C', 14, 16, 'shh3hh3h', 'gc789gcnbd45b', 3),
(6, 'C', 16, 20, 'kj1jfjdfkfklf6', 'gcgce3nbdb23', 4),
(7, 'C', 15, 4, 'ddhdhdhd', 'g5cgcnbd321b', 4),
(8, 'C', 9, 15, 'kj1jfjfkfklf6', 'gcgcn3u6bdb', 5),
(9, 'C', 3, 9, 'iiifjejjde', '112234de', 3),
(10, 'C', 12, 18, 'kjw1j2fjfkfklf', 'huy9gcgcn6bdb', 4),
(11, 'C', 19, 11, 'shhhh3h', 'gcgcnbdeetryrb', 6),
(12, 'C', 7, 7, 'shhhhh', '1g1fhtfhkk', 3),
(13, 'C', 8, 12, 'kj1jfjfkfklf', 'gcg2cnbdb234', 4),
(14, 'C', 1, 4, 'ddhdhdhd', '112234de', 3),
(15, 'C', 17, 1, '12345', 'gcgcnbdb345', 3),
(16, 'C', 5, 7, 'shhhhh', 'gcgcnbb', 5),
(17, 'C', 1, 15, 'kj1jfjfkfklf6', 'daerds', 3),
(18, 'C', 13, 17, 'kj1jfj43fkfklf', 'gcgcn34b1db', 7),
(19, 'C', 4, 3, 'jdfjdfkf', 'gfhfhkk', 3),
(20, 'C', 15, 4, 'ddhdhdhd', 'g5cgcnbd321b', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `CAMPAIGN`
--
ALTER TABLE `CAMPAIGN`
  ADD PRIMARY KEY (`CAM_ID`),
  ADD KEY `campaign_ibfk_1` (`C_ID`);

--
-- Indexes for table `CHARITY`
--
ALTER TABLE `CHARITY`
  ADD PRIMARY KEY (`C_ID`),
  ADD UNIQUE KEY `C_WALLET` (`C_WALLET`);

--
-- Indexes for table `DONOR`
--
ALTER TABLE `DONOR`
  ADD PRIMARY KEY (`D_ID`),
  ADD UNIQUE KEY `D_WALLET` (`D_WALLET`);

--
-- Indexes for table `PAYMENT`
--
ALTER TABLE `PAYMENT`
  ADD PRIMARY KEY (`PAY_ID`),
  ADD KEY `C_ID` (`C_ID`),
  ADD KEY `D_ID` (`D_ID`),
  ADD KEY `C_WALLET` (`C_WALLET`),
  ADD KEY `D_WALLET` (`D_WALLET`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `CAMPAIGN`
--
ALTER TABLE `CAMPAIGN`
  MODIFY `CAM_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `CHARITY`
--
ALTER TABLE `CHARITY`
  MODIFY `C_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `DONOR`
--
ALTER TABLE `DONOR`
  MODIFY `D_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `PAYMENT`
--
ALTER TABLE `PAYMENT`
  MODIFY `PAY_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
