<?php
$servername = "localhost";
$username = "sameera";
$password = "fOS4X2vkXpWWcmp8";
$db = "coingive";
$conn = new mysqli($servername, $username, $password, $db);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


$sql = "SELECT donor.Name as Name , payment.TOTAL_COIN as Amount FROM donor , payment , charity where donor.D_ID = payment.D_ID and charity.C_ID = payment.C_ID and charity.Name = 'salvos';";
$result = $conn-> query($sql);


require("fpdf17/fpdf.php");
class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    $this->Image('assets/images/sadaka.png',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'Reports',1,0,'C');
    // Line break
    $this->Ln(20);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

$pdf = new FPDF('p', 'mm', 'A4');

$pdf->AddPage();

$pdf->SetFont('Arial','B',14);

$pdf->cell(40,10,"Donor_Name", 1,0, 'C');

$pdf->cell(50,10,"Donation_Made", 1,1, 'C');

$pdf->SetFont('Arial','',14);

while ($row = $result-> fetch_assoc()){
	
	$pdf->cell(40,10,$row['Name'],1,0,'C');
	$pdf->cell(50,10,$row['Amount'],1,1,'C');
}

$pdf->Output();

$conn -> close();
?>